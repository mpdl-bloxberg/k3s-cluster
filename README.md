# cluster
This part of the project installs k3s on each machine of the cluster.
It also installs helm charts for:
- gitlab runner
- cert manager
- kubernetes dashboard

You can initiallize the cluster with: 
```
ansible-playbook playbook.yml -i inventory.yaml
```

Change the inventory file to configure the cluster as indicated in the comments.








# pipeline
contains a chart that is used for projects and scripts that are used to apply the chart and run other configs outside of kubernetes

## chart
- nginx
- services
- persistent volumes
- ports
- liveness checks

## scripts
- build into image
- save gitlab registry credentials
- update dns config


